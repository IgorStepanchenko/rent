import React, {Component} from 'react';
import './footer.css';
import fb from './img/fb.png';
import linked from './img/linked.png';
import instagram from './img/instagram.png';
import twitter from './img/twitter.png';




class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <footer className="Footer">
                <div className="Footer-Container Container">
                    <div className="Footer-Laptop">
                        <div className="Footer-ItemWrapp">
                            <h5 className="Footer-Title">Get started</h5>
                            <ul className="Footer-List">
                                <li><a href="#" className="Footer-Link">Rent a car</a></li>
                                <li><a href="#" className="Footer-Link">How it works</a></li>
                                <li><a href="#" className="Footer-Link">Got you covered</a></li>
                                <li><a href="#" className="Footer-Link">List you car</a></li>
                            </ul>
                        </div>
                        <div className="Footer-ItemWrapp">
                            <h5 className="Footer-Title">Learn more</h5>
                            <ul className="Footer-List">
                                <li><a href="#" className="Footer-Link">How rent my car works</a></li>
                                <li><a href="#" className="Footer-Link">Trust & safety</a></li>
                                <li><a href="#" className="Footer-Link">Login</a></li>
                                <li><a href="#" className="Footer-Link">Join us</a></li>
                            </ul>
                        </div>
                        <div className="Footer-ItemWrapp">
                            <h5 className="Footer-Title">Support</h5>
                            <ul className="Footer-List">
                                <li><a href="#" className="Footer-Link">Faq's</a></li>
                                <li><a href="#" className="Footer-Link">How it works</a></li>
                                <li><a href="#" className="Footer-Link">Renter help</a></li>
                                <li><a href="#" className="Footer-Link">Policies</a></li>
                            </ul>
                        </div>
                        <div className="Footer-ItemWrapp">
                            <h5 className="Footer-Title">Social</h5>
                            <ul className="Footer-List Footer-ListSocial">
                                <li><a href="#" className="Footer-Link"><img src={fb} alt="fb"/></a></li>
                                <li><a href="#" className="Footer-Link"><img src={twitter} alt="twitter"/></a></li>
                                <li><a href="#" className="Footer-Link"><img src={linked} alt="linked"/></a></li>
                                <li><a href="#" className="Footer-Link"><img src={instagram} alt="insta"/></a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="Footer-Mobile">
                        <h2 className="Footer-MobileMainTitle">Connect with us</h2>
                        <hr id="Hr"/>
                        <div className="Footer-MobileWrapp">
                            <div className="Footer-MobileLeft">
                                <h5 className="Footer-MobileTitle">Help desk</h5>
                                <span>03000-562-762</span>
                                <h5 className="Footer-MobileTitle">email us</h5>
                                <ul className="Footer-MobileList">
                                    <li><a href="#" className="Footer-MobileListLink">info@rentcar.pk</a></li>
                                    <li><a href="#" className="Footer-MobileListLink">Community Guideline</a></li>
                                    <li><a href="#" className="Footer-MobileListLink">Insurance</a></li>
                                </ul>
                            </div>
                            <div className="Footer-MobileRight">
                                <h5 className="Footer-MobileTitle">Quick links</h5>
                                <ul className="Footer-MobileList">
                                    <li><a href="#" className="Footer-MobileListLink">Terms & Conditions</a></li>
                                    <li><a href="#" className="Footer-MobileListLink">Policies</a></li>
                                    <li><a href="#" className="Footer-MobileListLink">Agreements</a></li>
                                    <li><a href="#" className="Footer-MobileListLink">List Your Car FAQs</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul className="Footer-MobileListSocial">
                        <li><a href="#" className="Footer-Link"><img src={fb} alt="fb"/></a></li>
                        <li><a href="#" className="Footer-Link"><img src={twitter} alt="twitter"/></a></li>
                        <li><a href="#" className="Footer-Link"><img src={linked} alt="linked"/></a></li>
                        <li><a href="#" className="Footer-Link"><img src={instagram} alt="insta"/></a></li>
                    </ul>
                </div>
            </footer>
        );
    }
}

export default Footer