import React, {Component} from 'react';
import './quotes.css';
import avatar from './img/avatar.png'
import star from './img/star.png'
import avatar_small from './img/avatar_small.png'


class Quotes extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <section className="Quotes">
                <div className="Quotes-Container Container">
                    <div className="Quotes-Wrapp">
                        <img src={avatar} alt="avatar" className="Quotes-Avatar"/>
                        <div className="Quotes-Content">
                            <p className="Quotes-Text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Accusantium, architecto, beatae doloremque eius expedita itaque libero minima natus nemo
                                non
                                perspiciatis sint tempore vero voluptatibus.</p>
                            <div className="Quotes-Signature">
                                <img src={avatar_small} alt="avatar-small" className="Quotes-AvatarSmall"/>
                                <div className="Quotes-SignatureWrapp">
                                    <span className="Quotes-SignatureBold">Sajid Hasan </span>
                                    <span>Co-founder at RentO</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="Quotes-StarsWrapp">
                        <div className="Quotes-StarsWrappFlex">
                            <div className="Quotes-StarCount">4.3</div>
                            <div className="Quotes-StarOtherInfo">
                                <span>4.3 of 5 stars</span>
                                <span>63 reviews</span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    };

}

export default Quotes