import React, {Component} from 'react';
import './partners.css';
import aroma from './img/aroma.png'
import echo from './img/echo.png'
import thenest from './img/thenest.png'
import vastech from './img/vastech.png'


class Partners extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <section className="Partners">
                <div className="Partners-Container Container">
                    <h2 className="Partners-Title">Our partners</h2>
                    <div className="Partners-ImgWrapp">
                        <img src={aroma} alt="aroma" className="Partners-Img"/>
                        <img src={thenest} alt="thenest" className="Partners-Img"/>
                        <img src={echo} alt="echo" className="Partners-Img"/>
                        <img src={vastech} alt="vastech" className="Partners-Img"/>
                    </div>
                </div>
            </section>
        );
    }
}

export default Partners