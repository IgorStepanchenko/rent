import React, { Component } from 'react';
import './rentmycar.css';

class RentMyCar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <section className="RentMyCar">
                <div className="RentMyCar-Container Container">
                    <h2 className="RentMyCar-Title">Rent my car for business</h2>
                    <p className="RentMyCar-Description">Let us handle your business account</p>
                    <a href="#" className="RentMyCar-Button">Learn more</a>
                </div>
            </section>
        );
    }
}

export default RentMyCar