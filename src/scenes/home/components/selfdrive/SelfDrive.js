import React, { Component } from 'react';
import './selfdrive.css';
import car from './img/car.png'
import mobile_phone from './img/mobile_phone.png'
import smile from './img/smile.png'


class SelfDrive extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <section className="SelfDrive">
                <div className="SelfDrive-Container Container">
                    <h2 className="SelfDrive-Title">Self drive rentals</h2>
                    <p className="SelfDrive-Description">Rent a car for self drive</p>
                    <div className="SelfDrive-ContentFlex">
                        <div className="SelfDrive-Item">
                            <img src={mobile_phone} alt="mobile_phone" className="SelfDrive-Img"/>
                            <div className="SelfDrive-ItemWrap">
                                <h2 className="SelfDrive-ItemTitle">Book</h2>
                                <p className="SelfDrive-ItemDescription">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
                            </div>
                        </div>
                        <div className="SelfDrive-Item">
                            <img src={car} alt="car" className="SelfDrive-Img"/>
                            <div className="SelfDrive-ItemWrap">
                                <h2 className="SelfDrive-ItemTitle">Pick up</h2>
                                <p className="SelfDrive-ItemDescription">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid deleniti eligendi incidunt numquam quaerat.</p>
                            </div>
                        </div>
                        <div className="SelfDrive-Item">
                            <img src={smile} alt="smile" className="SelfDrive-Img"/>
                            <div className="SelfDrive-ItemWrap">
                                <h2 className="SelfDrive-ItemTitle">Ride</h2>
                                <p className="SelfDrive-ItemDescription">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
                            </div>
                        </div>
                    </div>
                    <a href="#" className="SelfDrive-Button">Learn more</a>
                </div>
            </section>
        );
    }

}

export default SelfDrive;