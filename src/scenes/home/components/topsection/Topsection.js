import React, { Component } from 'react';
import './topsection.css';
import arrow_down from './img/arrow_down.png'

class TopSection extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <section className="TopSection">
                <div className="TopSection-Container Container TopSection-Container_flex">
                    <h1 className="TopSection-Title">Rent better cars</h1>
                    <p className="TopSection-Description">Choose from thousand of unique cars for rent by local
                        hosts</p>
                    <form className="TopSection-Form">
                        <input type="text" placeholder={'Location'} className="TopSection-Location"/>
                        <input type="text" placeholder={'From'} className="TopSection-DateFrom"/>
                        <input type="text" placeholder={'To'} className="TopSection-DateTo"/>
                        <button className="TopSection-Search"> Search for cars</button>
                    </form>
                </div>
            </section>
        );
    }
}

export default TopSection;