import React, { Component } from 'react';
import './twoimages.css';
import night_road from './img/night-road.png'
import sunset_road from './img/sunset-road.png'

class TwoImages extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <section className="TwoImages">
                <div className="TwoImages-Container Container">
                    <div className="TwoImages-Wrapp">
                        <img src={night_road} alt="night-road" className="TwoImages-Img"/>
                        <div className="TwoImages-Info">
                            <h2 className="TwoImages-Title">Out of city rentals</h2>
                            <p className="TwoImages-Description">travel across pakistan <br/> your advanture starts here</p>
                            <a href="#" className="TwoImages-Button">Learn more</a>
                        </div>
                    </div>
                    <div className="TwoImages-Wrapp">
                        <img src={sunset_road} alt="sunset-road" className="TwoImages-Img"/>
                        <div className="TwoImages-Info">
                            <h2 className="TwoImages-Title">Weekly, monthly $ <br/> long term rentals</h2>
                            <p className="TwoImages-Description">Don't buy car when you can rent it</p>
                            <a href="#" className="TwoImages-Button">Learn more</a>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default TwoImages