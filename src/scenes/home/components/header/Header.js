import React, {Component} from 'react';
import './header.css'
import logo from './img/logo.png';
import logo_min from './img/logo_min.png';
import user_logo from './img/user_logo.png'

class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <header className="Header">
                <div className="Header-Container Container Header-Container_flex">
                    <div className="Header-LeftContainer">
                        <div className="Header-Burger">
                            <span className="Header-Burger-Span"></span>
                            <span className="Header-Burger-Span"></span>
                            <span className="Header-Burger-Span"></span>
                            <span className="Header-Burger-Span"></span>
                        </div>
                        <a href="#" className="Header-Logo">
                            <img src={logo} alt="logo" className="Header-Img"/>
                            <img src={logo_min} alt="logo_min" className="Header-ImgMin"/>
                        </a>
                    </div>

                    <ul className="Header-Menu">
                        <li className="Header-Menu-Item"><a href="#" className="Header-Menu-Link">How it works</a></li>
                        <li className="Header-Menu-Item"><a href="#" className="Header-Menu-Link">Learn more</a></li>
                        <li className="Header-Menu-Item"><a href="#" className="Header-Menu-Link">List your car</a></li>
                        <li className="Header-Menu-Item"><a href="#" className="Header-Menu-Link">Contact us</a></li>
                        <li className="Header-Menu-Item"><a href="#" className="Header-Menu-Link">Sing in</a></li>
                    </ul>
                    <div className="Header-RightContainer">
                        <button className="Header-Button">List your car</button>
                        <a href="#" className="Header-UserLink"><img src={user_logo} alt="user_logo" className="Header-UserLogo"/></a>
                    </div>

                </div>
            </header>
        );
    }
}

export default Header;