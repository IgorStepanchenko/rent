import React, { Component } from 'react';
import './promoted.css';
import rentcar from './img/rentcar.jpg'


class Promoted extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <section className="Promoted">
                <div className="Promoted-Container Container">
                    <h2 className="Promoted-Title">Promoted cars</h2>
                    <p className="Promoted-Description">This is photoshop's version of lorem ipsum</p>
                    <div className="Promoted-SliderWrapp">
                        <div className="Promoted-Slide">
                            <img className="Promoted-CarLogo" src={rentcar} alt="rentcar"/>
                            <div className="Promoted-Discount">
                                <span className="Promoted-PromoCode">promo code: xyz</span>
                                <span className="Promoted-DiscountPercent">Discount: 10%</span>
                            </div>
                            <div className="Promoted-About">
                                <div className="Promoted-CarName">Honda vezel <span>2017</span></div>
                                <div className="Promoted-PriceWrapp">
                                    <span className="Promoted-Price"><sup className="Promoted-Sup">Rs.</sup> 6,000</span>
                                    <span className="Promoted-PerDay">Per Day</span>
                                </div>
                            </div>
                        </div>
                        <div className="Promoted-Slide">
                            <img className="Promoted-CarLogo" src={rentcar} alt="rentcar"/>
                            <div className="Promoted-Discount">
                                <span className="Promoted-PromoCode">promo code: xyz</span>
                                <span className="Promoted-DiscountPercent">Discount: 10%</span>
                            </div>
                            <div className="Promoted-About">
                                <div className="Promoted-CarName">Honda vezel <span>2017</span></div>
                                <div className="Promoted-PriceWrapp">
                                    <span className="Promoted-Price"><sup className="Promoted-Sup">Rs.</sup> 6,000</span>
                                    <span className="Promoted-PerDay">Per Day</span>
                                </div>
                            </div>
                        </div>
                        <div className="Promoted-Slide">
                            <img className="Promoted-CarLogo" src={rentcar} alt="rentcar"/>
                            <div className="Promoted-Discount">
                                <span className="Promoted-PromoCode">promo code: xyz</span>
                                <span className="Promoted-DiscountPercent">Discount: 10%</span>
                            </div>
                            <div className="Promoted-About">
                                <div className="Promoted-CarName">Honda vezel <span>2017</span></div>
                                <div className="Promoted-PriceWrapp">
                                    <span className="Promoted-Price"><sup className="Promoted-Sup">Rs.</sup> 6,000</span>
                                    <span className="Promoted-PerDay">Per Day</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Promoted;