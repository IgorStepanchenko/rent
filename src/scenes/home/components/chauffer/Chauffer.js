import React, { Component } from 'react';
import './chauffer.css';
import road_car from './img/road_car.png'

class Chauffer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <section className="Chauffer">
                <div className="Chauffer-Container Container">
                    <h2 className="Chauffer-Title">Chauffer driven rentals</h2>
                    <p className="Chauffer-Description">Rent a car with an experienced driver</p>
                    <div className="Chauffer-Round">
                        <span>convenience <br/>&<br/> availability</span>
                    </div>
                    <p className="Chauffer-SecondDescription">We let you manage your booking as per your convenience.</p>
                    <a href="#" className="Chauffer-Button">Learn more</a>
                    <img className="Chauffer-Img" src={road_car} alt="road-car"/>
                </div>
            </section>
        );
    }

}

export default Chauffer