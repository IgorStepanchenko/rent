import React, {Component} from 'react';
import './copyright.css';




class Copyright extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <section className="Copyright">
                <div className="Copyright-Container Container">
                    <p className="Copyright-Description"> 2018 Rent My Car all right reserved</p>
                </div>
            </section>
        );
    }
}

export default Copyright