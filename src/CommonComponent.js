import React, { Component } from 'react';
import Header from './scenes/home/components/header/Header';
import TopSection from './scenes/home/components/topsection/Topsection';
import Promoted from './scenes/home/components/promoted/Promoted';
import SelfDrive from './scenes/home/components/selfdrive/SelfDrive';
import Chauffer from './scenes/home/components/chauffer/Chauffer';
import TwoImages from './scenes/home/components/twoimages/TwoImages';
import Quotes from './scenes/home/components/quotes/Quotes';
import RentMyCar from './scenes/home/components/rentmycar/RentMyCar';
import Partners from './scenes/home/components/partners/Partners';
import Footer from './scenes/home/components/footer/Footer';
import Copyright from './scenes/home/components/copyright/Copyright';

class CommonComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div>
                <Header/>
                <TopSection/>
                <Promoted/>
                <SelfDrive/>
                <Chauffer/>
                <TwoImages/>
                <Quotes/>
                <RentMyCar/>
                <Partners/>
                <Footer/>
                <Copyright/>
            </div>
        );
    }
}

export default CommonComponent;