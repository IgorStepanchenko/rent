import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import CommonComponent from './CommonComponent';

import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<CommonComponent />, document.getElementById('root'));
registerServiceWorker();
